const express = require('express');
const fs = require('fs');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 3000;
const { MongoClient, ServerApiVersion } = require('mongodb');
const uri = "mongodb+srv://lucaspardina:Miranda13461@cluster0.cd5kary.mongodb.net/?retryWrites=true&w=majority";

app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

// Connect to the MongoDB database
client.connect((err) => {
  if (err) {
    console.error('Failed to connect to the database:', err);
    return;
  }
});

console.log('Connected to the database');
const db = client.db();

// Endpoint to get all reviews
app.get('/api/reviews', async (req, res) => {
  try {
    const reviews = await db.collection('reviews').find().toArray();
    res.json(reviews);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Endpoint to add a new review
app.post('/api/reviews', (req, res) => {
  const review = req.body;

  // Insert the review into the MongoDB collection
  const collection = client.db().collection('reviews');
  collection.insertOne(review, (err, result) => {
    if (err) {
      console.error(err);
      res.status(500).send('Failed to save the review.');
    } else {
      res.status(200).send('Review submitted successfully!');
    }
  });
});
/* app.post('/api/reviews', async (req, res) => {
const review = req.body;

  try {
    await db.collection('reviews').insertOne(review);
    res.status(200).json({ message: 'Review submitted successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
}); */

// Endpoint to delete a review
app.delete('/api/reviews/:id', async (req, res) => {
const id = req.params.id;

  try {
    await db.collection('reviews').deleteOne({ id });
    res.sendStatus(200);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Failed to delete the review' });
  }
});

// Catch-all route for serving the index.html file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
  });
  
  // Start the server
  app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
  });

  // Connect to MongoDB and start the server
/* client.connect((err) => {
  if (err) {
    console.error('Failed to connect to MongoDB:', err);
  } else {
    console.log('Connected to MongoDB');
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  }
}); */